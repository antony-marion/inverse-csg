         �   �      $��������=Q]M��`������L�]            x�mα
�@����%i4�Z��Q���9����;.{)�ݠB,,���0y�ش�Y29;+�J�i:Y��d���m7v�1�٢T^z�{�@Ɍ��]������;GCR-l�f�sԄ���ci�w� ��a�Q ;�JQi[X���/jH��l�u�Ҽ �'Rd     �     \   �      )    �����s�C/�{�Sm?�Q�V|�            x�c``(d``����e�E%��9
I��9
�%���U�e��%�E6P*3���NM!1'�3����XG!�8'"�����Y\�X���`�``� �/     �     =        *   �����8������G)�H��uv.               �   �   1	virtual void randomizeCtrls(gsl_vector* x) = 0;
    ;     3  G     -   ����_0J'�e.毈�i�w	c�                   '	virtual double getObjectiveVal() = 0;
    n     r  c     /   �����������/�Zk��h���            x�c``(d``8���e�E%��9
I��9
�%���U�e��%�E6P*3���NM!1'�3����XG!�8'"�����Y\�X��1�����(��8��O�V!-1�8U�0�� ��)q    �     �  �     <   �����oj�"y|�a��������!            x�m�A
1E�N=EV2�,܊�lŝ��:U�-If��G��\�$�����2YOH�!kk	N!��xÇ���ت�;k�U��k]O�e����1�3@�zP���i���Nd��*�X7M�|96f0LZ$�����l��o�b�LR��K���G��~G�    }     )  �     h   ����L�1��t!Mn����x�               )   L   #include "NumericalSolver.h"
    �      �     k   ����{5�����&%����'O��            x�mPMK1=�_1� �,~\m-�x�V���2���#�$$�E����RzHཙ���}��EpV���&��&��|�iq��c6�F�����ʙ��p��6#,�p��(��[@�)���2�o�I�A��3�v�ṰPؠ�s`����cY	
�����d�Vf�;0��ٚ��}��B&�6hb�`q��~y�?�2}}�����h?�!Y��F�{�������%�X�k�J8򜣍�k�����e�����W4�~'��E�"    �     m  �     l   ����G�/ q(�\�?�4'���              %  �   a	virtual void randomizeCtrls(gsl_vector* x, Interface* inputs, const set<int>& constraints) = 0;
         �  �     n   ������|�^��6����(5XG�-            x�5��
1���Sl%*Wh�XX\!ȝ��Ͻ#���\�C��������a�R7��K�9L�-\�����y�t���-�����
��6��cf�k֌��!����䫣!�O)�"�@�@�-I0���ܜ�r_^�칻����}&�b5~�>�    �     �  �   	  o   	�����\p~K 	�q���̓��            x����1�����I�=p�'qp bܤvK�6�ٍxO��L-���Ц3�f~��'!jM!Ľ^�@�4�w΀�����sK*d����/8�ٕ*#bOSJ�T��H����ҸU�Ar�3|Ù�s�rƟ1Xx�*D`iPU�b��m��Y����ikis׼�?z�{L���?.��9��Ĕ�����/	����� ��o    }     ,  �     v   ����Z��/�v&����3/.���            x�c``�e``�bF. ��"\�9���
��9�e��%�E�\\ f �    �     �  �     x   
   (
����:���6��F?]%            x����JA��G=�B� U�� ��t{�LgS��Y��"��	��ۊ/�L��c��1c��]��p�[$�l�S�.���
�3Xh�8�=I��)H��ֿ�hic���]���êG'���K-V��"0ʕ�rs���j�;��U�ܜ�F�o�]G��H����[��v�Z̧����;���mlR9����q9Q/�w�O��?^����ƝP����V���9��\�z#    �     .  �     y   ����������ߏ
��e;:j�(�                  +   "
#include <set>

class Interface;
    �     "       z   ����x����#����ϙ^�ܺ                     using namespace std;

    �     �  2   
  �   
���������(�e�c*��Ʊ��            x���;
1�c���J|l����Pĵ�����`L���"��3yo����	��7�?B�����Z-��B�:g����SK�g2Q-�6/#ءٔ*!�COSL�T��H����Ұ�����/SIr�	�V��]�zwX�W��dҠzг�z�ZN'1��O<�YJ�� �|��j4�u��w����kbL�`�3�1�?�ߨO7���    �     �  G     �   ����'u�D�r�����D����ˆ            x�e�?1�먟"�����qTD�$w�I��M���l���K�{���RW�k�ԣy3Aj��yo�WbJs���	�s�qU-܇��r�!yF�B}Ƚc&�'��[�����@�Pƴ�t�u�{�ສ1�B�P�ezӛ��|ܯ��h�>x��G�}����י%�Xf�=�i�[O�xWp    s    A  �     �   ����R+����̒/��*�e��>            x���OO1���h 1h�x$!���%��̶�tۦ�n���n+l��if�k��u��}	�h�YGj��@6�{5��BN��_&��^���r�*�b;˸��ʒ,�;�4�с���GfC�$��Z�t@�������&�I-)' �L>X�������ہ�QK�b�������^O7�E~=ѡ�:���:��-L�i^��)(q=�/�5�� ε������'X�!;��#-��������vVF
��9���[��5����(��1n�'��~rq���Q ��Ԑ
��]J\�5U�t��W�k�>���?G��R�    	�      I     �   �����3�q�/�۹8�,�]���            x����J1�So|����҃��DT
Z�+��MR�i�df�|�Gr�m��x�df��7��'��r�B�m`��f\ڠ����񮼗�s�r�M���]XCP��� �B��+D�2�r��`�u�k���TS!w8&�1�����u���-F�&�q�ft���uC8�r�QI������L]��iW'���	g�"j�o�`S�� >�,��<�N�p��z^�oJn_��y�R3f/sr^����G����K�������q�����0�/'����x��    
�     �  �     �   ��������%p6�~9<�o��X �            x�5��
�@E;���D���Gll[�$Y؇����Lw)���ù�����;lBώ�y~[o?��H긑���[jbP�S�c�1bqC&���8-�U%�*m�\e?'����kl�ԹȠZ�#�#�H���nMG��?D�=T    ]    |       �   ����s�U V�Q�C�m�?oe�UBG�            x��R�J�0��5Oq��h��6�����b�"NA�1��j0kJ��B�W>���C�`��e��4����w��8���ص�')���s��|�cMᅔU�YwCS-d����3�)�
a<�3P�C.$d,ϩ�����X�u=��E*
-�<�Z����k���&!nQs��pX�ܝ�aY��U�O^��*��v*�"iK���i�wŶ�b��jwtrI���޳e+��&X��ʍP**Q=�;�u0 z��V35�-7Y���O������ӏ�ѷd����8{o��&u��'m�fk�T�BS��)��e���������G��(���g�:�1&�X�lj^��VFUe)�RW��uqz�����-F��4�ط��/����    �      V     �   ����bT2�L�����j�j;�0            x����J1@G����6'�e=["
*ŊxJ6;�@v�df��O�!����`�у�a�Lx�� XA���#-ΏK��&H�;��[ k�)`'���'6O�W$��Z�x	�[�/��X8g�5b��Nk!�T�Fh����"M�wF梄rԮfA&�Z&���*�M8�
p�y�ʀ�c�m���| uyz������s�pZپ��W�0KY<���a8���`s\^�T��W���?�3y9��ki��?���Sit��,�[e�i|| "���    �     �  X     �   ����r��>3�L��cGd��u�            x�5��
�@���B�b+���?���[�\6�p�w� �����y��,�3���0�೨�I��d���bOO����S.P�0j,�x��'6�CNDk%�Y����B�����(Y�O�����o�YO�+�x܊I[I��Y�����p�[~q;B�    �       j     �   �����zdw�I9��t|����               P   P   #include "Util.h"
    �     �       �   �����v*/�w��7���b}�[�            x�M�=!FG���
�d��Ů^���A�*���q�W�Lb(�x���| >PX�d4��_�J���D*Ҟ��1��T�"V���H���M���\����)���E��8 �!ra=q˷|�����6٧2ϛF���*}�ƈ��hOY��f_A�?�    F     ;  K     �   �������
EΪ��?�����              a  a   /	virtual const set<int>& getConstraints() = 0;
    �     �  �     �   �����ϥz���|�`WXX�i6P            x�=�M
�0��=ŬDK����.�I�Qi2�.<�7�
�ȫ���f�7|��|�o �*���t�u��j�YV�.�JP[_2�� s��d������H����r�� ���8��p��w��75Tz�!D��v���t��y�['�i�4�}��Ά �@��,A�    0     �  �     �      �
E���:H��N��            x�}�KjA�z�Z��>q!$!���f�z���!���*9���*���ঞ=>����7 ���R�Ü�!WbK�e�3/&�Sa2���%*��n՘B8�5+!1
�Q0��^o�y����d�L�v��$ߝ�uU�kH���y��Z�gӅ�'.���g莱B�u�:*sM����Ѡuw�����Ow8�Cj�j�5��'\�R|*���"�yS������-�~P         _       �   �����8vrZ��E��!}GO���K              �  �   G	virtual bool optimize(gsl_vector* initState, bool suppressPrint) = 0;
  �  �        v    8  6     �   ����g@P�s������[��?y��            x�}Q�N�0B?a	ڪb�:�0\`0e��UJ�*N�aڗ��nөb����~���K�_"��mYk���W[5U<l'rʫ��;�ŏ�F�g�ʩ
=:��lڵ)�L��+��Gw��u��asL^��X�.��S`ڶB�w����.Gv5OA�S�T�fN��@�Þ�@{g(�׼�FiL�&��$������k<�i��_�F��V鿾�}�c�.\���ͬ��Q$:���H�"q:PG�y�+p�F-��I�ԫ��I^�J�[e��P�s�:���CAf|&@^y��3�FC��